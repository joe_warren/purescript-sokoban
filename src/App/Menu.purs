module App.Menu where

import Prelude

import App.Grid as G
import App.Puzzle as P
import Data.Maybe (Maybe(..), fromMaybe)
import Effect.Aff (Aff)
import Halogen (ClassName)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

type Entry = { name :: String, puzz :: P.Puzzle, hiScore :: Maybe String}

type State = Array Entry
type Output = Entry
data Action = Raise Output

type Slot id = forall q. H.Slot q Output id

component :: forall q i. State -> H.Component HH.HTML q i Output Aff
component is =
  H.mkComponent
    { initialState: \_ -> is
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }
    
entryclass :: Entry -> ClassName
entryclass e = case e.hiScore of
  Nothing -> HH.ClassName $ "menuitem nowin"
  Just _ -> HH.ClassName "menuitem won"

render :: forall cs. State -> H.ComponentHTML Action cs Aff
render state = HH.div [HP.class_ $ HH.ClassName "menu" ] 
    [ HH.h1_ [HH.text "PureScript::Sokoban"]
    , HH.div [HP.class_ (HH.ClassName "menucontent")]
        (
            (\entry -> HH.button
                [ HP.class_ (entryclass entry)
                , HE.onClick \_ -> pure $ Raise entry
                ]
                [ HH.h2_ [HH.text entry.name]
                 , HH.p_ [HH.text $ fromMaybe "" ((\s -> "Current High Score: " <> s <> " turns") <$> entry.hiScore)]
                , G.grid entry.puzz
                ]
            ) 
        <$> state
        )
  ]
    
handleAction :: forall cs. Action -> H.HalogenM State Action cs Output Aff Unit
handleAction = case _ of
  Raise output -> H.raise output 


