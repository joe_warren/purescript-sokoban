module App.Game where

import Prelude

import App.Grid as GR
import App.Puzzle as P
import App.VictoryModal as VictoryModal
import Data.Array ((:))
import Data.Int (toNumber)
import Data.Ord (abs)
import Data.List.NonEmpty as NE
import Data.Either (Either(..))
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Tuple (Tuple(..))
import Effect.Aff (Aff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource (eventListenerEventSource)
import Icons as Icons
import Web.HTML (window) as Web
import Web.HTML.HTMLDocument as HTMLDocument
import Web.HTML.Window (document) as Web
import Web.Event.Event as EV
import Web.UIEvent.KeyboardEvent as KE
import Web.TouchEvent.TouchEvent as TE
import Web.TouchEvent.TouchList as TEL
import Web.UIEvent.KeyboardEvent.EventTypes as KET
import Web.TouchEvent.Touch as T
import Effect.Console (log)

type State = { gameHistory :: NE.NonEmptyList P.Puzzle, touchedAt :: Maybe (Tuple Int Int) }
data Action = Init | Move P.Direction | Undo | Reset | TouchStart (Tuple Int Int) | TouchEnd (Tuple Int Int) | PreventDefault TE.TouchEvent |Exit 
data Output = Victory Int | NoVictory
type Slot id = forall q. H.Slot q Output id

initialState :: P.Puzzle -> State
initialState p = { gameHistory: NE.singleton p, touchedAt: Nothing }

modifyGameHistory :: (NE.NonEmptyList P.Puzzle -> NE.NonEmptyList P.Puzzle) -> State -> State
modifyGameHistory f s = s {gameHistory = f s.gameHistory }

touchPosition :: TEL.TouchList -> Maybe (Tuple Int Int)
touchPosition tl = (\t -> Tuple (T.screenX t) (T.screenY t))  <$> TEL.item 0 tl

touchStart :: TE.TouchEvent -> Maybe Action
touchStart = map TouchStart <<< touchPosition <<< TE.touches 

touchEnd :: TE.TouchEvent -> Maybe Action
touchEnd = map TouchEnd <<< touchPosition <<< TE.changedTouches 

component :: forall q i. P.Puzzle -> H.Component HH.HTML q i Output Aff
component level =
  H.mkComponent
    { initialState: \_ -> initialState level
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction, initialize = Just Init }
    }

render :: forall cs. State -> H.ComponentHTML Action cs Aff
render state = HH.div [HP.class_ $ HH.ClassName "game" ] $
    grid : controls : modal
  where 
    grid = HH.div [HP.class_ (HH.ClassName "grid"), HE.onTouchEnd touchEnd, HE.onTouchStart touchStart, HE.onTouchMove (Just <<< PreventDefault ) ] [GR.grid puzz]
    controls = HH.div [HP.class_ (HH.ClassName "gamecontrols")]
                [ HH.button 
                    [HP.class_ (HH.ClassName "exit")
                    , HE.onClick \_ -> pure $ Exit
                    ] 
                    [ HH.span_ [
                            HH.text "quit"
                        ]
                    ,Icons.iconExit []
                    ]
                , HH.button 
                    [HP.class_ (HH.ClassName "reset")
                    , HE.onClick \_ -> pure $ Reset
                    ] 
                    [ HH.span_ [
                            HH.text "reset"
                        ]
                    ,Icons.iconReset []
                    ]
                , HH.button 
                    [HP.class_ (HH.ClassName "undo")
                    , HE.onClick \_ -> pure $ Undo
                    ] 
                    [ HH.span_ [
                            HH.text "undo"
                        ]
                    ,Icons.iconUndo []
                    ]
                ]
    puzz = NE.head state.gameHistory
    turns = NE.length state.gameHistory
    modal = if P.isWon puzz 
      then pure $ VictoryModal.modal (turns) (Exit)
      else mempty


handleKey :: KE.KeyboardEvent -> Maybe Action
handleKey k = case KE.key k of
  "ArrowUp" -> Just $ Move P.North
  "ArrowRight" -> Just $ Move P.East
  "ArrowDown" -> Just $ Move P.South
  "ArrowLeft" -> Just $ Move P.West
  "w" -> Just $ Move P.North
  "d" -> Just $ Move P.East
  "s" -> Just $ Move P.South
  "a" -> Just $ Move P.West
  "u" -> Just Undo
  "q" -> Just Exit
  "r" -> Just Reset
  _ -> Nothing

calcDirection :: Tuple Int Int -> Tuple Int Int -> Either String P.Direction
calcDirection (Tuple x1 y1) (Tuple x2 y2) = 
  let dx = x2 - x1
      dy = y2 - y1
      d2 = dx * dx + dy*dy
      rat = (toNumber $ min (abs dx) (abs dy)) / (toNumber $ max (abs dx) (abs dy))
  in if d2 < 25*25
        then Left "too small"
        else if rat > 0.5
            then Left "too angled"
            else if abs dx > abs dy  
              then if dx > 0
                then Right P.East
                else Right P.West
              else if dy > 0
                then Right P.South
                else Right P.North


handleAction :: forall cs. Action -> H.HalogenM State Action cs Output Aff Unit
handleAction = case _ of
  Init -> do
    document <- H.liftEffect $ Web.document =<< Web.window
    H.subscribe' \sid ->
      eventListenerEventSource
        KET.keyup
        (HTMLDocument.toEventTarget document)
        (KE.fromEvent >=> handleKey)
  Move d -> 
    H.modify_ (modifyGameHistory $ \st -> fromMaybe st $ NE.cons  <$> (P.tryMove d (NE.head st)) <*> pure st)
  Undo -> 
    H.modify_ (modifyGameHistory $ \st -> fromMaybe st $ NE.fromList (NE.tail st))
  Reset -> 
    H.modify_ (modifyGameHistory $ NE.singleton <<< NE.last)
  Exit -> do 
    state <- H.get
    let turns = NE.length state.gameHistory
    let puzz = NE.head state.gameHistory
    if P.isWon puzz 
      then H.raise $ Victory turns 
      else H.raise NoVictory
  TouchStart pos -> do
    H.modify_ (\st -> st {touchedAt = Just pos})
  TouchEnd pos -> do
    st <- H.get
    case st.touchedAt of
      Nothing -> pure unit
      Just ta -> case calcDirection ta pos of
                  Left msg -> H.liftEffect $ log msg
                  Right m -> handleAction $ Move m
  PreventDefault ev -> H.liftEffect (EV.preventDefault (TE.toEvent ev))
        