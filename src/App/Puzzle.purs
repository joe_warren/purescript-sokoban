module App.Puzzle where

import Prelude 
import Data.Tuple (Tuple (..))
import Data.Array (head, elem, mapWithIndex) 
import Data.String.Common (split)
import Data.String.CodeUnits (toCharArray)
import Data.String.Pattern (Pattern (..))
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Foldable (and)
import Control.MonadZero (guard)


data Direction = North | East | South | West

type Position = Tuple Int Int

type Puzzle = 
    { player :: Position
    , boxes :: Array Position
    , targets :: Array Position
    , walls :: Array Position
    }


level1 :: String 
level1 = """
    #####          
    #   #          
    #$  #          
  ###  $##         
  #  $ $ #         
### # ## #   ######
#   # ## #####  ..#
# $  $          ..#
##### ### #@##  ..#
    #     #########
    #######        
"""
parseMap :: String -> Puzzle
parseMap s = 
  { player: fromMaybe (Tuple 1 1) (head $ f '@')
  , boxes : f '$'
  , targets: f '.'
  , walls : f '#'
  }
  where
    ind :: forall a. Array a -> Array (Tuple Int a)
    ind = mapWithIndex Tuple
    f :: Char -> Array (Tuple Int Int)
    f a = do
      Tuple y line <- ind $ toCharArray <$> split (Pattern "\n") s
      Tuple x c <- ind line
      guard $ c == a
      pure $ Tuple x y 


move :: Direction -> Position -> Position
move d (Tuple x y) = case d of 
  North -> Tuple (x) (y-1)
  South -> Tuple (x) (y+1)
  East -> Tuple (x+1) (y)
  West -> Tuple (x-1) (y)

tryMove :: Direction -> Puzzle -> Maybe Puzzle
tryMove d s = do
  let newPos = move d s.player
  _ <- guard $ (not $ elem newPos s.walls)
  if elem newPos s.boxes 
    then do  
      let pushOnto = move d newPos
      _ <- guard $ (not $ elem pushOnto s.walls)
      _ <- guard $ (not $ elem pushOnto s.boxes)
      let newBoxes = (\b -> if b == newPos then pushOnto else b) <$> s.boxes
      Just $ s { player = newPos, boxes = newBoxes }
  else Just $ s {player = newPos}

isWon :: Puzzle -> Boolean
isWon p = and $ (_ `elem` p.targets) <$> p.boxes