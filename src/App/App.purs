module App.App where

import Prelude

import App.Game (Output(..))
import App.Game as G
import App.Levels as L
import App.Menu as M
import App.Puzzle as P
import Data.Int (fromString)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String (toLower, replace, Replacement (..), Pattern (..))
import Data.Symbol (SProxy(..))
import Data.Traversable (traverse)
import Data.Tuple (Tuple(..), fst)
import Data.Array (find)
import Effect (Effect)
import Effect.Console (log)
import Effect.Aff (Aff)
import Halogen as H
import Halogen.HTML as HH
import Web.HTML (window)
import Web.HTML.Window (localStorage, location)
import Web.HTML.Location (hash, setHash)
import Web.Storage.Storage as WS

data State = Menu M.State | Game String P.Puzzle
data Action = Play M.Output | Return G.Output

type Slots = ( menu :: M.Slot Unit, game :: G.Slot Unit )
_menu :: SProxy "menu"
_menu = SProxy
_game :: SProxy "game"
_game = SProxy

levelNameAsHash :: String -> String
levelNameAsHash =  ("#" <> _) <<< (replace (Pattern " ") (Replacement "_")) <<< toLower

initialState :: Effect State
initialState = do
    h <- window >>= location >>= hash
    let f = find ((_ == h) <<< levelNameAsHash <<< fst) L.levels
    log $ show h
    log $ show f
    case f of 
      Just (Tuple n p) -> pure $ Game n p
      Nothing -> loadScores

component :: forall q i o. Effect (H.Component HH.HTML q i o Aff)
component  = do
  is <- initialState
  pure $ H.mkComponent
    { initialState: \_ -> is
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

render :: State -> H.ComponentHTML Action Slots Aff
render (Menu s) = HH.div_ [
        HH.slot _menu unit (M.component s) unit (Just <<< Play)  
    ]
render (Game name puzz) = HH.div_ [
        HH.slot _game unit (G.component puzz) unit (Just <<< Return)  
    ]

getHiScore :: String -> Effect (Maybe String)
getHiScore level = do
  w <- window
  store <- localStorage w
  WS.getItem level store

loadScores :: Effect State
loadScores = Menu <$> traverse loadScore L.levels
  where
    loadScore (Tuple name puzz) = do
      s <- getHiScore name
      pure { name: name, puzz: puzz, hiScore: s}  

setHiScore :: forall a s o. Int -> H.HalogenM State a s o Aff Unit
setHiScore score =  do
    s <- H.gets identity
    H.liftEffect $ case s of 
      Menu _ -> pure unit
      Game name _ -> do
        w <- window
        store <- localStorage w
        prevScore <- (_ >>= fromString) <$> WS.getItem name store
        if fromMaybe true ((_ > score) <$> prevScore) 
          then WS.setItem name (show score) store
          else pure unit

handleAction :: forall o. Action -> H.HalogenM State Action Slots o Aff Unit
handleAction action = do
  l <- H.liftEffect $ window >>= location
  case action of
    Play e -> do
      H.liftEffect $ setHash (levelNameAsHash e.name) l
      H.put $ Game e.name e.puzz
    Return result -> do
      case result of
        (Victory turns) -> setHiScore turns
        _ -> pure unit
      H.liftEffect $ setHash "" l
      levels <- H.liftEffect loadScores
      H.put levels
