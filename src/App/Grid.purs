module App.Grid where

import Prelude
import App.Puzzle 

import Data.Array(cons, elem, singleton)
import Data.Maybe (fromMaybe)
import Data.Tuple (Tuple, fst, snd)
import Data.Foldable (minimum, maximum)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Halogen.HTML.Elements as HHE

type Action = Unit

-- todo, nonemptify this
allIndices :: Puzzle -> Array Position
allIndices s = s.player `cons` s.boxes <> s.targets <> s.walls

type Bounds = 
  { minX :: Int
  , minY :: Int
  , maxX :: Int
  , maxY :: Int
  }

bounds :: Puzzle -> Bounds
bounds s = let ixs = allIndices s in 
    { minX: fromMaybe 0 $ minimum $ fst <$> ixs
    , minY: fromMaybe 0 $ minimum $ snd <$> ixs
    , maxX: fromMaybe 0 $ maximum $ fst <$> ixs
    , maxY: fromMaybe 0 $ maximum $ snd <$> ixs
   }

viewbox :: Bounds -> String
viewbox b = (show b.minX) <> " " <> show b.minY <> " " <> show (1 + b.maxX - b.minX) <> " " <> show (1 + b.maxY - b.minY)

svgns :: HH.Namespace
svgns = HH.Namespace "http://www.w3.org/2000/svg"

position :: forall i w. HH.HTML i w -> Tuple Int Int -> HH.HTML i w
position child p = 
  HHE.elementNS svgns (HH.ElemName "g") 
    [ HP.attr (HH.AttrName "style") ("transform: translate(" <> (show $ fst p) <> "px, " <> (show $ snd p) <> "px)")
    ,HP.attr (HH.AttrName "class") "entity"
    ] 
    (singleton child)

playerElement :: forall i w. HH.HTML i w
playerElement = 
  HHE.elementNS svgns (HH.ElemName "circle")
    [HP.attr (HH.AttrName "r") "0.5"
    ,HP.attr (HH.AttrName "cx") "0.5"
    ,HP.attr (HH.AttrName "cy") "0.5"
    ,HP.attr (HH.AttrName "class") "player"
    ] []

rectElement :: String -> forall i w. HH.HTML i w
rectElement s = 
  HHE.elementNS svgns (HH.ElemName "rect")
    [HP.attr (HH.AttrName "width") "1"
    ,HP.attr (HH.AttrName "height") "1"
    ,HP.attr (HH.AttrName "class") s
    ] []

grid :: forall a cs e. Puzzle -> H.ComponentHTML a cs e
grid state =
  HH.div_
    [
    HHE.elementNS svgns (HH.ElemName "svg") 
        [ HP.attr (HH.AttrName "viewBox") (viewbox $ bounds state), 
          HP.attr (HH.AttrName "preserveAspectRatio") "xMidYMid meet"
        ] 
        ((position (rectElement "wall") <$> state.walls)
          <> (position (rectElement "target") <$> state.targets)
          <> (boxElement <$> state.boxes)
          <> [position playerElement state.player]
        )
    ]
  where 
    boxElement b = 
      if elem b state.targets  
        then position (rectElement "boxActive") b
        else position (rectElement "boxInactive") b

  
