module App.VictoryModal where

import Prelude
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP

modal :: forall a cs e. Int -> a -> H.ComponentHTML a cs e
modal turns onclick = HH.div
    [HP.class_ $ HH.ClassName "victory"]
    [
        HH.h2_ [HH.text "Victory"], 
        HH.p_ [HH.text $ "in " <> show turns <> " turns"], 
        HH.button [ 
            HE.onClick \_ -> pure onclick
          ] [ HH.text "return" ]
    ]