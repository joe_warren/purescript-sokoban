{ name = "halogen-project"
, dependencies =
  [ "console", "effect", "halogen", "psci-support", "read", "web-storage" ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
