module.exports = {
  plugins: [
    { removeViewBox: false }, 
    { removeUnknownsAndDefaults: false,
        SVGid: false
    }, 
    { removeUselessDefs: false },
    { mergePaths: false }, 
    { convertShapeToPath: false },
    { convertElipseToCircle: false },
    { convertTransform: false },
    { convertPathData: false },
    { collapseGroups: false },
    { cleanupIDs: false },
    { cleanUpIDs: false },
    { removeHiddenElems: false },
    { sortAttrs: false },
    {
      removeAttrs: {
        attrs: []
      }
    }
  ]
};
